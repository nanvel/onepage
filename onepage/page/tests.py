from django.test import TestCase
from django.core.urlresolvers import reverse


class ViewsTestCase(TestCase):

    def test_index(self):
        r = self.client.get(reverse('index'))
        self.assertEqual(r.status_code, 200)
