from django.conf.urls import patterns, include, url
from django.contrib import admin


admin.autodiscover()


urlpatterns = patterns('',
    url(r'^$', 'onepage.page.views.index', name='index'),
    url(r'^admin/', include(admin.site.urls)),
)

handler404 = 'onepage.page.views.handler404'
